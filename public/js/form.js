const FORM_ID = "registerForm";
const COUNTRIES_FILE = "data/countries.json";
const ERROR_MESSAGES_FILE = "data/errorMessages.json";
let form;
let errorMessages;
let formState = { touched: undefined, errors: undefined };

const validators = {
  emptyField: (value) => value !== "",
  invalidName: (value) => /^[\p{L} ,.'-]+$/u.test(value),
  invalidDate: (value) => value === "" || (new Date(value) <= Date.now() && new Date(value) >= new Date("1899-01-01")),
  invalidPhone: (value) => /^\+?\d{1,5}?[-.\s]?\(?\d{1,5}?\)?[-.\s]?\d{1,5}[-.\s]?\d{1,5}[-.\s]?\d{1,9}$/i.test(value),
  invalidEmail: (value) => /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/i.test(value),
  shortPassword: (value) => value.length >= 7,
  invalidPassword: (value) => /^[\w~`! @#$%^&*()_\-+={[}\]|:;"'<,>.?\\/]+$/i.test(value),
  passwordContainsPassword: (value) => !/password/i.test(value),
  passwordsNotEqual: (value, name) => {
    if (name === "password") {
      validate({ target: form.elements.passwordRepeat });
      return true;
    }
    return form.elements.password.value === value;
  },
};

const validatorNames = {
  firstName: ["emptyField", "invalidName"],
  lastName: ["emptyField", "invalidName"],
  dateOfBirth: ["invalidDate"],
  phone: ["emptyField", "invalidPhone"],
  email: ["emptyField", "invalidEmail"],
  password: ["passwordsNotEqual", "emptyField", "shortPassword", "invalidPassword", "passwordContainsPassword"],
  passwordRepeat: ["passwordsNotEqual"],
};

/* ----------------- MAIN ----------------- */

initForm();

async function initForm() {
  form = document.getElementById(FORM_ID);

  setupDateOfBirth();
  await setupCountries();
  await loadErrorMessages(ERROR_MESSAGES_FILE);
  setupFormState();

  form.addEventListener("submit", onSubmit);
  form.addEventListener("input", validate);
}

function onSubmit(event) {
  event.preventDefault();

  // Sanity check
  validateAll();
  if (formState.errors.size !== 0) return;
  const formData = new FormData(form);
  formData.delete("passwordRepeat");
  const data = {};
  formData.forEach((value, key) => data[key] = value);
  console.log(data);
  alert('Registration complete!');
}

function validate({ target, doNotTouch }) {
  let { name, value, id } = target;
  const errorNode = document.querySelector(`#${FORM_ID} #${id} ~ span.error-warning`);

  if (!validatorNames[name] || !errorNode) return;

  value = value.trim();
  if (!doNotTouch) formState.touched.add(name);

  let errorMessage = "";
  for (const validator of validatorNames[name]) {
    const isOk = validators[validator](value, name);
    if (!isOk) {
      formState.errors.add(name + "-" + validator);
      if (validator !== "emptyField" || formState.touched.has(name)) { // Do not set error message for untouched empty fields
        target.classList.add("border-danger");
        errorMessage = errorMessages[validator] || "";
      }
      break;
    } else {
      target.classList.remove("border-danger");
      formState.errors.delete(name + "-" + validator);
    }
  }
  errorNode.innerText = errorMessage;
  adjustSubmitBtn();
}

function validateAll(touch = true) {
  Object.values(form.elements).forEach((element) => {
    if (!element.name) return;
    validate({ target: element, doNotTouch: !touch});
  });
}

async function loadErrorMessages(fileName) {
  try {
    const data = await fetch(fileName);
    errorMessages = await data.json();
  } catch (error) {
    console.error("Error loading error messages data:", error);
  }
  errorMessages = errorMessages || {};
}

async function setupCountries() {
  const list = await loadCountries(COUNTRIES_FILE);
  const country = form.elements.country;
  country.innerHTML = list.map(({ name }) => `<option value="${name}">${name}</option>\n`);
  country.selectedIndex = -1;
}

async function loadCountries(fileName) {
  try {
    const data = await fetch(fileName);
    const json = await data.json();
    return json;
  } catch (error) {
    console.error("Error loading countries data:", error);
  }
}

function setupDateOfBirth() {
  form.elements.dateOfBirth.max = new Date().toISOString().split("T")[0];
}

function setupFormState() {
  formState.touched = new Set();
  formState.errors = new Set();
  validateAll(false);
}

function adjustSubmitBtn() {
  const btn = document.querySelector(`#${FORM_ID} button[type="submit"]`);
  if (!btn) return;
  console.log(formState.errors);
  btn.toggleAttribute("disabled", formState.errors.size !== 0);
}
